# Yew Frontend

This project was done in conjunction with the gamified backend project. It was separated from the backend because of compatibility between actix and yew in the same project.
It was never version controlled and was developed locally.

This version of the yew project used the old struct based system. Analogous to the old React class based system. There was an recent update were it is encouraged to use
the function based version. This somewhat aligns it with how React modern function based version looks.
