// From components/_progress-bar.scss
pub const PROGRESS_BAR_1: &str = r#"
    background-color: #a4b0be;
    border-radius: 1rem;
    width: 120rem;
"#;

pub const PROGRESS_BAR_2: &str = r#"
    content: "";
    display: table;
    clear: both;
    background-color: #2ed573;
    color: #2ed573;
    border-radius: 1rem;
"#;

pub const POINTS: &str = r#"
    font-size: 3rem;
    font-weight: 900;
"#;
