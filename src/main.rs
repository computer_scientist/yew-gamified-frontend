use yew::prelude::*;
use yew_router::prelude::*;

mod elements_lesson;
mod styles;

use console_log;
use elements_lesson::ElementsLesson;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/elements_lesson/:id")]
    ElementsLesson { id: u32 },
}

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::ElementsLesson { id } => html! {<ElementsLesson id={id}/>},
        Route::Home => html! { <ElementsLesson id={1}/> },
    }
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} />
        </BrowserRouter>
    }
}

fn main() {
    console_log::init_with_level(log::Level::Debug).expect("Error initializing log");
    yew::Renderer::<App>::new().render();
}
