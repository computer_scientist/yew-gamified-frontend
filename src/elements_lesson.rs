use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::sync::{Arc, Mutex};
use wasm_bindgen_futures::spawn_local;
use web_sys::window;
use yew::callback;
use yew::html::Scope;
use yew::prelude::*;
use yew_router;

use crate::styles;

#[derive(Serialize, Deserialize)]
pub struct PointsData {
    game_name: String,
    points: i64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct UsernameResponse {
    username: String,
}

pub async fn post_points(data: PointsData) -> Result<(), Box<dyn Error>> {
    let client = Client::new();
    let res = client
        .post("https://capstone.rasinnovation.org:8080/process_points_science")
        // Local
        // .post("https://localhost/process_points")
        .form(&data)
        .send()
        .await?;

    if res.status().is_success() {
        Ok(())
    } else {
        Err(format!("Received non-Ok status code: {}", res.status()).into())
    }
}

struct PageAnswer {
    correct_answer: String,
}

pub struct ElementsLesson {
    id: u32, // This will be an internal ID for the lesson
    // username: Arc<Mutex<Option<String>>>,
    // username: String,
    page: u32,
    points: i64,
    answer_checked: bool,
    correct_answer_given: bool,
    selected_answer: Option<String>,
    correct_answers: Vec<PageAnswer>,
    selected_tag: Option<usize>,
}

pub enum Msg {
    // Define a new message variant for handling correct answers
    SelectAnswer(String),
    CheckAnswer,
    NextPage,
    BonusNextPage,
    ProcessPoints,
    Exit,
    Stay,
}

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub id: u32,
}

impl ElementsLesson {
    fn is_answer_correct(&self) -> bool {
        if let Some(selected_answer) = &self.selected_answer {
            return self.correct_answers.iter().any(|page_answer| {
                page_answer.correct_answer.to_uppercase() == selected_answer.to_uppercase()
            });
        }
        false
    }
}

impl Component for ElementsLesson {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let id = ctx.props().id;
        let page = 1;
        let points = 0; // Start with 0 points
        let answer_checked = false;
        let correct_answer_given = false;
        let selected_answer = None;
        let selected_tag = None;
        // let username = String::from("testuser1");

        // let username = Arc::new(Mutex::new(None));
        // let username_clone = Arc::clone(&username);
        //
        // spawn_local(async move {
        //     let response = reqwest::get("/get-username").await;
        //     match response {
        //         Ok(resp) => {
        //             let username_resp: UsernameResponse =
        //                 resp.json().await.expect("Failed to parse username");
        //             *username_clone.lock().unwrap() = Some(username_resp.username);
        //         }
        //         Err(err) => log::error!("Failed to fetch username: {:?}", err),
        //     }
        // });

        Self {
            id,
            // username,
            page,
            points,
            answer_checked,
            correct_answer_given,
            selected_answer,
            correct_answers: vec![
                PageAnswer {
                    correct_answer: "A TYPE OF ELEMENT".to_string(),
                },
                PageAnswer {
                    correct_answer: "ATOMIC NUMBER".to_string(),
                },
                PageAnswer {
                    correct_answer: "NUMBER OF PROTONS".to_string(),
                },
                PageAnswer {
                    correct_answer: "A NICKNAME".to_string(),
                },
                PageAnswer {
                    correct_answer: "THE FULL NAME OF THE ELEMENT".to_string(),
                },
                PageAnswer {
                    correct_answer: "HOW HEAVY THE ATOM IS".to_string(),
                },
            ],
            selected_tag,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SelectAnswer(answer) => {
                if !self.answer_checked {
                    self.selected_answer = Some(answer);
                }
            }
            Msg::CheckAnswer => {
                self.answer_checked = true;

                if self.is_answer_correct() {
                    self.points += 10;
                    self.correct_answer_given = true;
                }
            }
            Msg::NextPage => {
                let total_pages = 9;
                if self.page < total_pages {
                    self.page += 1;
                    self.selected_answer = None;
                    self.answer_checked = false;
                    self.selected_tag = None;
                    self.correct_answer_given = false;
                }
            }
            Msg::BonusNextPage => {
                let total_pages = 9;
                if self.page < total_pages {
                    self.points *= 2;
                    self.page += 1;
                    self.selected_answer = None;
                    self.answer_checked = false;
                    self.selected_tag = None;
                    self.correct_answer_given = false;
                }

                let points = self.points;
                let game_name = String::from("science");
                let points_data = PointsData { game_name, points };

                spawn_local(async move {
                    match post_points(points_data).await {
                        Ok(_) => {
                            log::info!("Points posted successfully");
                        }
                        Err(err) => {
                            log::info!("Error posting points: {:?}", err);
                        }
                    }
                });
            }
            Msg::ProcessPoints => {
                // let username = self.username.lock().unwrap().clone().unwrap();
                // let username = self.username.clone();
                let points = self.points;
                let game_name = String::from("science");
                let points_data = PointsData { game_name, points };

                let window = window().unwrap();
                let location = window.location();

                spawn_local(async move {
                    match post_points(points_data).await {
                        Ok(_) => {
                            log::info!("Points posted successfully");
                            let _ = location.set_href("/activities");
                        }
                        Err(err) => {
                            log::info!("Error posting points: {:?}", err);
                            let _ = location.set_href("/activities");
                        }
                    }
                });
            }
            Msg::Exit => {
                let window = window().unwrap();
                let location = window.location();
                let _ = location.set_href("/activities");
            }
            Msg::Stay => {}
        }
        true // Indicate that the component should re-render
    }

    fn changed(&mut self, ctx: &Context<Self>, _old_props: &Self::Properties) -> bool {
        let old_id = self.id;
        let new_id = ctx.props().id;
        self.id = new_id;
        old_id != new_id
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let total_pages = 9;
        let completion_percentage = (self.page * 100) / total_pages;
        let progress_bar_2_style = format!("width: {}%;", completion_percentage);

        let content = match self.page {
            1 => render_page1_content(self.points, &progress_bar_2_style, ctx.link()),
            2 => render_page2_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            3 => render_page3_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            4 => render_page4_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            5 => render_page5_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            6 => render_page6_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            7 => render_page7_content(
                self.points,
                &progress_bar_2_style,
                ctx.link(),
                self.selected_answer.as_ref(),
                self.page,
                self.answer_checked,
                &self.correct_answers,
            ),
            8 => render_page8_content(self.points, &progress_bar_2_style, ctx.link()),
            9 => render_page9_content(self.points, &progress_bar_2_style, ctx.link()),
            _ => render_unknown_page_content(),
        };
        html! {
            <>
                {content}
            </>
        }
    }
}

fn get_answer_tag_class(
    selected_answer: Option<&String>,
    correct_answer: &str,
    current_answer: &str,
    answer_checked: bool,
) -> &'static str {
    if answer_checked {
        match selected_answer {
            Some(answer)
                if answer.to_uppercase() == current_answer.to_uppercase()
                    && answer.to_uppercase() == correct_answer.to_uppercase() =>
            {
                "btn-2 btn-choice correct-answer"
            } // selected and correct
            Some(answer) if answer.to_uppercase() == current_answer.to_uppercase() => {
                "btn-2 btn-choice wrong-answer"
            } // selected but not correct
            _ if current_answer.to_uppercase() == correct_answer.to_uppercase() => {
                "btn-2 btn-choice correct-answer"
            } // not selected, but is correct
            _ => "btn-2 btn-choice", // none of the above, so normal state
        }
    } else {
        match selected_answer {
            Some(answer) if answer.to_uppercase() == current_answer.to_uppercase() => {
                "btn-2 btn-choice lesson-button-selected"
            }
            _ => "btn-2 btn-choice",
        }
    }
}

fn check_answer_switcher(link: &Scope<ElementsLesson>, answer_checked: bool) -> Html {
    {
        if answer_checked {
            html! {
                <button class="btn btn-next" onclick={link.callback(|_| Msg::NextPage)}>
                    { "Next Page" }
                </button>
            }
        } else {
            html! {
                <button class="btn btn-lesson" onclick={link.callback(|_| Msg::CheckAnswer)}>
                    { "Check Answer" }
                </button>
            }
        }
    }
}

fn render_common_top_container(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
) -> Html {
    html! {
        <>
            <section class="lesson-top-elements">
                <a href="#" onclick={link.callback(|_| if window().unwrap().confirm_with_message("Are you sure you want to exit? You will lose all your progress in current lesson!").unwrap() {Msg::Exit} else {Msg::Stay})}>
                    <img src="/img/close_white.png" alt="home" class="close-game" />
                </a>
                <div class="progress-container">
                    <div style={styles::PROGRESS_BAR_1}>
                        <div style={format!("{}{}", styles::PROGRESS_BAR_2, progress_bar_2_style)}>{"."}</div>
                    </div>
                </div>
                <div class="points-container">
                    <img
                        src="/img/sparkles.png"
                        alt="sparkles"
                        class="sparkle-icon" />
                    <p style={styles::POINTS}>{ points }</p>
                </div>
            </section>
        </>
    }
}

fn render_page1_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
) -> Html {
    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container">
                <h1 class="header-lesson-main text-shadow">{"Welcome to our Chemistry Adventure"}</h1>
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"We are going to learn about the building blocks of everything around us: Elements!"}
                    </p>
                    <img src="/img/Globe.png" alt="" class="lesson-image-2" />
                    <p class="activity-paragraph">{"Are you ready to start your Chemistry Adventure"}</p>
                </div>
                <button class="btn btn-lesson" onclick={link.callback(|_| Msg::NextPage)}>
                    { "Yes" }
                </button>
            </section>
        </>
    }
}

fn render_page2_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
           <>
                { render_common_top_container(points, progress_bar_2_style, link) }

                <section class="lesson-content-container mt-5">
                    <div class="lesson-content">
                        <p class="activity-paragraph">
                            {"Everything around us is made of tiny"}
                            <span class="lesson-bold">{" particles"}</span>{" called"}
                            <span class="lesson-bold">{" atoms"}</span>{"."}
                        </p>
                        <p class="activity-paragraph">
                            {"These"}<span class="lesson-bold">{" atoms"}</span>{" are all sorts
                            of different"}
                            <span class="lesson-bold">{" types"}</span>{"."}
                        </p>
                        <p class="activity-paragraph">
                            {"Each"}<span class="lesson-bold">{" type"}</span>{" is called an"}
                            <span class="lesson-bold">{" Element"}</span>{"!"}
                        </p>
                        <img src="/img/Atom_Label.png" alt="" class="lesson-image-2" />
                        <p class="activity-question">{"What is an atom?"}</p>
                    </div>
                    <div class="question-selections">
                        <a
                            href="#"
                            class={get_answer_tag_class(selected_answer, correct_answer, "A type of element", answer_checked)}
                            onclick={link.callback(|_| Msg::SelectAnswer("A TYPE OF ELEMENT".to_string()))}>
                            {"A type of element"}
                        </a>
                        <a
                            href="#"
                            class={get_answer_tag_class(selected_answer, correct_answer, "A small molecule", answer_checked)}
                            onclick={link.callback(|_| Msg::SelectAnswer("A SMALL MOLECULE".to_string()))}>
                            {"A small molecule"}
                        </a>
                        <a
                            href="#"
                            class={get_answer_tag_class(selected_answer, correct_answer, "A big molecule", answer_checked)}
                            onclick={link.callback(|_| Msg::SelectAnswer("A BIG MOLECULE".to_string()))}>
                            {"A big molecule"}
                        </a>
                    </div>
                    { check_answer_switcher(link, answer_checked) }
                </section>
           </>
    }
}

fn render_page3_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container mt-5">
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"Each"}<span class="lesson-bold">{" Element"}</span>{" has its own
                        special card with important information on it."}
                    </p>
                    <p class="activity-paragraph">
                        {"Let's look at the first piece of information: The"}
                        <span class="lesson-bold">{" Atomic Number"}</span>{"."}
                    </p>
                    <img src="/img/Gold_AN.png" alt="" class="lesson-image-2" />
                    <p class="activity-question">
                        {"What is the first piece of information on the element card?"}
                    </p>
                </div>
                <div class="question-selections">
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "Element Name", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("ELEMENT NAME".to_string()))}>
                        {"Element Name"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "Atomic Number", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("ATOMIC NUMBER".to_string()))}>
                        {"Atomic Number"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "Atomic Mass", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("ATOMIC MASS".to_string()))}>
                        {"Atomic Mass"}
                    </a>
                </div>
                { check_answer_switcher(link, answer_checked) }
            </section>
        </>
    }
}

fn render_page4_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container mt-5">
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"The"}<span class="lesson-bold">{" Atomic Number"}</span>{" tells us how many"}
                        <span class="lesson-bold">{" protons"}</span>{" are in the"}
                        <span class="lesson-bold">{" nucleus"}</span>{" of the atom."}
                    </p>
                    <p class="activity-paragraph">
                        {"The"}<span class="lesson-bold">{" nucleus"}</span>{" is like the atom's"}
                        <span class="lesson-bold">{" heart"}</span>{"!"}
                    </p>
                    <img
                        src="/img/Nucleus_Atom.png"
                        alt=""
                        class="lesson-image-2" />
                    <p class="activity-question">
                        {"What does the Atomic Number tell us?"}
                    </p>
                </div>
                <div class="question-selections">
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "Number of protons", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("NUMBER OF PROTONS".to_string()))}>
                        {"Number of protons"}
                    </a>
                    <a
                        class={get_answer_tag_class(selected_answer, correct_answer, "Size of the atom", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("SIZE OF THE ATOM".to_string()))}>
                        {"Size of the atom"}
                    </a>
                    <a
                        class={get_answer_tag_class(selected_answer, correct_answer, "Number of electrons", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("NUMBER OF ELECTRONS".to_string()))}>
                        {"Number of electrons"}
                    </a>
                </div>
                { check_answer_switcher(link, answer_checked) }
            </section>
        </>
    }
}

fn render_page5_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container mt-5">
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"The next piece of information on the"}
                        <span class="lesson-bold">{" card"}</span>{" is the"}
                        <span class="lesson-bold">{" Element Symbol"}</span>{"."}
                    </p>
                    <p class="activity-paragraph">
                        {"This is like a"}<span class="lesson-bold">{" nickname"}</span>{" of
                        the element."}
                    </p>
                    <p class="activity-paragraph">{"For example, Oxygen is \"O\"."}</p>
                    <img src="/img/Oxygen_1.png" alt="" class="lesson-image-2" />
                    <p class="activity-question">{"What is the Element symbol?"}</p>
                </div>
                <div class="question-selections">
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "A long name", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("A LONG NAME".to_string()))}>
                        {"A long name"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "A nickname", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("A NICKNAME".to_string()))}>
                        {"A nickname"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "A number", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("A number".to_string()))}>
                        {"A number"}
                    </a>
                </div>
                { check_answer_switcher(link, answer_checked) }
            </section>
        </>
    }
}

fn render_page6_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container mt-5">
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"The"}<span class="lesson-bold">{" Element Name"}</span>{" is the"}
                        <span class="lesson-bold">{" full name"}</span>{" of the"}
                        <span class="lesson-bold">{" element"}</span>{"."}
                    </p>
                    <p class="activity-paragraph">
                        {"Like \"Hydrogen\" and \"Oxygen\""}
                    </p>
                    <div class="lesson-image-duo-container">
                        <img
                            src="/img/Hydrogen_1.png"
                            alt=""
                            class="lesson-image-2" />
                        <img
                            src="/img/Oxygen_2.png"
                            alt=""
                            class="lesson-image-2" />
                    </div>
                    <p class="activity-question">{"What is the Element Name?"}</p>
                </div>
                <div class="question-selections">
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "The Full Name of the Element", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("THE FULL NAME OF THE ELEMENT".to_string()))}>
                        {"The Full Name of the Element"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "The Nickname of the Element", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("THE NICKNAME OF THE ELEMENT".to_string()))}>
                        {"The Nickname of the Element"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "The Number of the Element", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("THE NUMBER OF THE ELEMENT".to_string()))}>
                        {"The Number of the Element"}
                    </a>
                </div>
                { check_answer_switcher(link, answer_checked) }
            </section>
        </>
    }
}

fn render_page7_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
    selected_answer: Option<&String>,
    page: u32,
    answer_checked: bool,
    correct_answers: &Vec<PageAnswer>,
) -> Html {
    let correct_answer_index = page as usize - 2;
    let correct_answer = &correct_answers[correct_answer_index].correct_answer;

    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container mt-5">
                <div class="lesson-content">
                    <p class="activity-paragraph">
                        {"The final piece of information on the element card is the"}
                        <span class="lesson-bold">{" Atomic Mass"}</span>{"."}
                    </p>
                    <p class="activity-paragraph">
                        {"This tell how"}<span class="lesson-bold">{" heavy"}</span>{" the"}
                        <span class="lesson-bold">{" atom"}</span>{" is."}
                    </p>
                    <img src="/img/Lead_AM.png" alt="" class="lesson-image-2" />
                    <p class="activity-question">
                        {"What does the atomic mass tell us?"}
                    </p>
                </div>
                <div class="question-selections">
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "How many protons that are in the atom", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("HOW MANY PROTONS THAT ARE IN THE ATOM".to_string()))}>
                        {"How many protons that are in the atom"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "How heavy the atom is", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("HOW HEAVY THE ATOM IS".to_string()))}>
                        {"How heavy the atom is"}
                    </a>
                    <a
                        href="#"
                        class={get_answer_tag_class(selected_answer, correct_answer, "The nickname of the atom", answer_checked)}
                        onclick={link.callback(|_| Msg::SelectAnswer("THE NICKNAME OF THE ATOM".to_string()))}>
                        {"The nickname of the atom"}
                    </a>
                </div>
                { check_answer_switcher(link, answer_checked) }
            </section>

        </>
    }
}

fn render_page8_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
) -> Html {
    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="lesson-content-container">
                <div class="lesson-content">
                    <h1 class="header-lesson-main">{"Great Job!"}</h1>
                    <p class="activity-question">
                        {"Now you know how to read an element card."}
                    </p>
                    <p class="activity-question">
                        {"Let's explore the elements in the next lesson."}
                    </p>
                    <img src="/img/Card_Collection.png" alt="" class="lesson-image-2" />
                    <p class="lesson-bold">{"Bonus!"}</p>
                    <p class="activity-question">
                        {"Are you excited to learn about different elements in our
                        next lesson?"}
                    </p>
                </div>
                <button class="btn btn-lesson" onclick={link.callback(|_| Msg::BonusNextPage)}>
                    { "Yes" }
                </button>
            </section>
        </>
    }
}

fn render_page9_content(
    points: i64,
    progress_bar_2_style: &str,
    link: &Scope<ElementsLesson>,
) -> Html {
    html! {
        <>
            { render_common_top_container(points, progress_bar_2_style, link) }

            <section class="congratulations-container">
                <h1 class="header-lesson-main">{"Congratulations!"}</h1>
                <p>{"You Scored"}</p>
                <div class="total-points">
                    <img src="/img/sparkles.png" alt="" class="sparkle-icon" />
                    <p>{ points }{" points"}</p>
                    <img src="/img/sparkles.png" alt="" class="sparkle-icon" />
                </div>
                <button class="btn btn-lesson" onclick={link.callback(|_| Msg::Exit)}>
                    { "Return to Activities Page" }
                </button>
            </section>
        </>
    }
}

fn render_unknown_page_content() -> Html {
    html! {
        <section class="lesson-content-container">
            <h1>{ "Page not found" }</h1>
        </section>
    }
}
